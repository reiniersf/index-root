package cu.uci.cegel.onei.classifiers;


import cu.uci.cegel.onei.classifiers.domain.*;
import cu.uci.cegel.onei.classifiers.infrastructure.data.Set4;
import cu.uci.cegel.onei.classifiers.infrastructure.function.ArithmeticFunctions;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import static cu.uci.cegel.onei.classifiers.domain.ClassifierComponents.newRootComponent;
import static cu.uci.cegel.onei.classifiers.domain.Currency.CUC;
import static cu.uci.cegel.onei.classifiers.domain.Currency.CUP;
import static cu.uci.cegel.onei.classifiers.domain.WeightType.RUBRO;
import static cu.uci.cegel.onei.classifiers.domain.WeightType.TOTAL;
import static java.util.Collections.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Tests {

    private WeightSpec defaultWeightSpec = WeightSpec.of(TOTAL, CUP);

    @Test
    public void shouldReturnOptionalEmpty() {
        assertThat(Set4.EMPTY.filter(e -> true)).isEqualTo(Optional.empty());
    }

    @Test
    public void shouldNotAllowSameElementMultipleTimes() {
        assertThatThrownBy(() -> Set4.of(
                AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUP),
                AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUP),
                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)
        )).isExactlyInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void shouldGetZeroAsIndexValueWhenNoSubComponents() {
        AggregationComponent aggregationComponent = newRootComponent(
                "01",
                Set4.of(
                        AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUP),
                        AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUC),
                        AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                        AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)
                ),
                emptySet());

        assertThat(aggregationComponent).isNotNull();
        assertThat(aggregationComponent.indexValue(defaultWeightSpec)).isZero();

    }

    @Test
    public void shouldGetNonZeroAsIndexValueWhenHasSubComponents() {

        AggregationComponent aggregationComponentA = mock(AggregationComponent.class, "A");
        when(aggregationComponentA.indexValue(defaultWeightSpec)).thenReturn(BigDecimal.valueOf(11.32));
        when(aggregationComponentA.weightValueForSpec(defaultWeightSpec)).thenReturn(BigDecimal.TEN);

        AggregationComponent aggregationComponentB = mock(AggregationComponent.class, "B");
        when(aggregationComponentB.indexValue(defaultWeightSpec)).thenReturn(BigDecimal.valueOf(38.3));
        when(aggregationComponentB.weightValueForSpec(defaultWeightSpec)).thenReturn(BigDecimal.TEN);

        AggregationComponent aggregationComponentC = mock(AggregationComponent.class, "C");
        when(aggregationComponentC.indexValue(defaultWeightSpec)).thenReturn(BigDecimal.valueOf(26.21));
        when(aggregationComponentC.weightValueForSpec(defaultWeightSpec)).thenReturn(BigDecimal.TEN);

        AggregationComponent aggregationComponentD = mock(AggregationComponent.class, "D");
        when(aggregationComponentD.indexValue(defaultWeightSpec)).thenReturn(BigDecimal.valueOf(23.17));
        when(aggregationComponentD.weightValueForSpec(defaultWeightSpec)).thenReturn(BigDecimal.TEN);

        AggregationComponent aggregationComponent = newRootComponent(
                "01",
                Set4.of(
                        AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUP),
                        AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUC),
                        AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                        AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)
                ),
                new HashSet<>(Arrays.asList(
                        newRootComponent("0101", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(19.3), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(16.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(35.7), RUBRO, CUC)),
                                singleton(aggregationComponentA)),
                        newRootComponent("0102", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(20.31), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(27.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(45.7), RUBRO, CUC)),
                                singleton(aggregationComponentB)),
                        newRootComponent("0103", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(22.3), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)),
                                singleton(aggregationComponentC)),
                        newRootComponent("0104", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(23.3), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)),
                                singleton(aggregationComponentD))
                )));

        assertThat(aggregationComponent).isNotNull();
        BigDecimal indexValue = aggregationComponent.indexValue(defaultWeightSpec);
        System.out.println("Index final value = " + indexValue);
        assertThat(indexValue).isGreaterThanOrEqualTo(BigDecimal.valueOf(24.88));

    }

    @Test
    public void shouldGetNonZeroAsIndexValueWhenHasSubComponentsIncludingBaseComponents() {

        AggregationComponent baseComponentA = ClassifierComponents.newBaseComponent(
                "code",
                ArithmeticFunctions.RATIO,
                ArithmeticFunctions.GEOMETRIC_MEDIA,
                Arrays.asList(
                        Unit.of("01", Arrays.asList(
                                RelatedPrices.of(4.35, 2.40),
                                RelatedPrices.of(3.75, 2.40),
                                RelatedPrices.of(4.15, 2.40)
                        )),
                        Unit.of("02", singletonList(
                                RelatedPrices.of(7.39, 8.05)
                        ))
                ));

        AggregationComponent baseComponentB = ClassifierComponents.newBaseComponent(
                "code",
                ArithmeticFunctions.RATIO,
                ArithmeticFunctions.GEOMETRIC_MEDIA,
                Arrays.asList(
                        Unit.of("01", singletonList(
                                RelatedPrices.of(4.35, 2.40)
                        )),
                        Unit.of("02", singletonList(
                                RelatedPrices.of(7.39, 8.05)
                        ))
                ));

        AggregationComponent baseComponentC = ClassifierComponents.newBaseComponent(
                "code",
                ArithmeticFunctions.RATIO,
                ArithmeticFunctions.GEOMETRIC_MEDIA,
                Arrays.asList(
                        Unit.of("01", Arrays.asList(
                                RelatedPrices.of(4.35, 2.40),
                                RelatedPrices.of(3.75, 2.40)
                        )),
                        Unit.of("02", singletonList(
                                RelatedPrices.of(7.39, 8.05)
                        ))
                ));

        AggregationComponent baseComponentD = ClassifierComponents.newBaseComponent(
                "code",
                ArithmeticFunctions.RATIO,
                ArithmeticFunctions.GEOMETRIC_MEDIA,
                Arrays.asList(
                        Unit.of("01", Arrays.asList(
                                RelatedPrices.of(4.35, 2.40),
                                RelatedPrices.of(3.75, 2.40)
                        )),
                        Unit.of("02", singletonList(
                                RelatedPrices.of(7.39, 8.05)
                        ))
                ));

        AggregationComponent aggregationComponent = newRootComponent(
                "01",
                Set4.of(
                        AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUP),
                        AggregationWeight.with(BigDecimal.valueOf(20.3), TOTAL, CUC),
                        AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                        AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)
                ),
                new HashSet<>(Arrays.asList(
                        newRootComponent("0101", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(19.3), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(16.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(35.7), RUBRO, CUC)),
                                singleton(baseComponentA)),
                        newRootComponent("0102", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(20.31), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(27.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(45.7), RUBRO, CUC)),
                                singleton(baseComponentB)),
                        newRootComponent("0103", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(22.3), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)),
                                singleton(baseComponentC)),
                        newRootComponent("0104", Set4.of(
                                AggregationWeight.with(BigDecimal.valueOf(23.3), TOTAL, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(1), TOTAL, CUC),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUP),
                                AggregationWeight.with(BigDecimal.valueOf(15.7), RUBRO, CUC)),
                                singleton(baseComponentD))
                )));

        assertThat(aggregationComponent).isNotNull();
        BigDecimal indexValue = aggregationComponent.indexValue(defaultWeightSpec);
        System.out.println("Index final value = " + indexValue);
        assertThat(indexValue).isGreaterThanOrEqualTo(BigDecimal.ONE);

    }

    @Test
    public void should() {

        //GIVEN
        AggregationComponent aBaseComponent = ClassifierComponents.newBaseComponent(
                "code",
                ArithmeticFunctions.RATIO,
                ArithmeticFunctions.GEOMETRIC_MEDIA,
                Arrays.asList(
                        Unit.of("code", singletonList(
                                RelatedPrices.of(4.35, 2.40)
                        )),
                        Unit.of("code", singletonList(
                                RelatedPrices.of(7.39, 8.05)
                        ))
                ));

        //WHEN
        BigDecimal microIndex = aBaseComponent.indexValue(WeightSpec.of(TOTAL, CUP));

        //THEN
        //1.8125 0.9180
        //
        System.out.println("Micro index final value = " + microIndex);
        assertThat(microIndex).isGreaterThanOrEqualTo(BigDecimal.valueOf(1.2899127));


    }


}
