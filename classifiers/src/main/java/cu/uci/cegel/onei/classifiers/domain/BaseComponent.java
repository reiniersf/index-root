package cu.uci.cegel.onei.classifiers.domain;

import cu.uci.cegel.onei.classifiers.infrastructure.function.ArithmeticFunctions;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class BaseComponent implements AggregationComponent {

    private final String code;
    private final BinaryOperator<Double> priceRelationFunction;
    private final Function<List<BigDecimal>, BigDecimal> relativeReduction;
    private final Function<List<BigDecimal>, BigDecimal> baseIndexFunction;
    private final List<Unit> variations;

    public BaseComponent(String code,
                         BinaryOperator<Double> priceRelationFunction,
                         Function<List<BigDecimal>, BigDecimal> relativeReduction,
                         Function<List<BigDecimal>, BigDecimal> baseIndexFunction,
                         List<Unit> variations) {
        this.code = code;
        this.priceRelationFunction = priceRelationFunction;
        this.relativeReduction = relativeReduction;
        this.baseIndexFunction = baseIndexFunction;
        this.variations = variations;
    }

    public BaseComponent(String code,
                         BinaryOperator<Double> priceRelationFunction,
                         Function<List<BigDecimal>, BigDecimal> baseIndexFunction,
                         List<Unit> variations) {
        this.code = code;
        this.priceRelationFunction = priceRelationFunction;
        this.relativeReduction = ArithmeticFunctions.ARITHMETIC_MEDIA;
        this.baseIndexFunction = baseIndexFunction;
        this.variations = variations;
    }

    @Override
    public BigDecimal indexValue(WeightSpec weightSpec) {
        return baseIndexFunction.apply(
                variations.stream()
                        .map(unit -> unit.priceRelative(priceRelationFunction, relativeReduction))
                        .collect(Collectors.toList())
        );
    }

    @Override
    public BigDecimal weightValueForSpec(WeightSpec weightSpec) {
        return BigDecimal.ONE;
    }

    @Override
    public String code() {
        return code;
    }


}
