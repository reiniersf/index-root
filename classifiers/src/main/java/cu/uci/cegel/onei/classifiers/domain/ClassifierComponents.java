package cu.uci.cegel.onei.classifiers.domain;

import cu.uci.cegel.onei.classifiers.infrastructure.data.Set4;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public final class ClassifierComponents {

    public static AggregationComponent newRootComponent(String code, Set4<AggregationWeight> weights,
                                                        Set<AggregationComponent> subComponents) {
        return new RootComponent(code, weights, subComponents);
    }

    public static AggregationComponent newBaseComponent(String code, BinaryOperator<Double> priceRelationFunction,
                                                        Function<List<BigDecimal>, BigDecimal> relativeReduction,
                                                        Function<List<BigDecimal>, BigDecimal> indexFunction,
                                                        List<Unit> units) {
        return new BaseComponent(code, priceRelationFunction, relativeReduction, indexFunction, units);
    }

    public static AggregationComponent newBaseComponent(String code, BinaryOperator<Double> priceRelationFunction,
                                                        Function<List<BigDecimal>, BigDecimal> indexFunction,
                                                        List<Unit> units) {
        return new BaseComponent(code, priceRelationFunction, indexFunction, units);
    }
}
